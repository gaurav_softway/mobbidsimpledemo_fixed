//
//  main.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
