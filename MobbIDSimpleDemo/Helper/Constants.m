//
//  Constants.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 11/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "Constants.h"

/**
 * Extras
 */

NSString *const EXTRA_USER_ID = @"com.mobbeel.mobbid.EXTRA_USER_ID";

/**
 * Biometric methods.
 */
NSString *const METHOD_IRIS = @"IRIS";
NSString *const METHOD_FACE = @"FACE";
NSString *const METHOD_SIGNATURE = @"SIGNATURE";
NSString *const METHOD_VOICE = @"VOICE";
NSString *const METHOD_VOICE_FACE = @"VOICE_FACE";

NSString *const BTN_ENABLED = @"enabled";
NSString *const BTN_DISABLED = @"disabled";
NSString *const BTN_V = @"v";
NSString *const BTN_X = @"x";
NSString *const BTN_DRAG = @"drag";

NSString *const ENROLL_METHODS[] = { @"FACE", @"IRIS", @"SIGNATURE", @"VOICE" };
NSString *const VERIFY_METHODS[] = { @"FACE", @"IRIS", @"SIGNATURE", @"VOICE", @"VOICE_FACE" };

int const RESULT_OK = 1;
int const RESULT_CANCEL = 2;
