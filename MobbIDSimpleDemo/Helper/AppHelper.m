//
//  AppHelper.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "AppHelper.h"

@implementation AppHelper

+ (NSString *)appendPlatform:(NSString *)obj {
	NSString *platform = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? @"_iPhone" : @"_iPad";
	return [obj stringByAppendingString:platform];
}

+ (MobbIDAPISupportedLanguage)languageFromDevice {
	MobbIDAPISupportedLanguage language = MobbIDAPISupportedLanguage_English; // default value.
	NSString *deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
	if ([deviceLanguage isEqualToString:@"es"]) {
		language = MobbIDAPISupportedLanguage_Spanish;
	}
	else if ([deviceLanguage isEqualToString:@"en"]) {
		language = MobbIDAPISupportedLanguage_English;
	}
	else if ([deviceLanguage isEqualToString:@"de"]) {
		language = MobbIDAPISupportedLanguage_German;
	}
	else if ([deviceLanguage isEqualToString:@"fr"]) {
		language = MobbIDAPISupportedLanguage_French;
	}
	else if ([deviceLanguage isEqualToString:@"ru"]) {
		language = MobbIDAPISupportedLanguage_Russian;
	}
	return language;
}

@end
