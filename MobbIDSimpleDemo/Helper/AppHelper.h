//
//  AppHelper.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MobbIDFramework/MobbIDFramework.h>

@interface AppHelper : NSObject

+ (NSString *)appendPlatform:(NSString *)obj;

+ (MobbIDAPISupportedLanguage)languageFromDevice;

@end
