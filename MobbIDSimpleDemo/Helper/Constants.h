//
//  Constants.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 11/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Extras
 */

extern NSString *const EXTRA_USER_ID;

/**
 * Biometric methods.
 */
extern NSString *const METHOD_IRIS;
extern NSString *const METHOD_FACE;
extern NSString *const METHOD_SIGNATURE;
extern NSString *const METHOD_VOICE;
extern NSString *const METHOD_VOICE_FACE;

extern NSString *const ENROLL_METHODS[];
extern NSString *const VERIFY_METHODS[];

extern int const RESULT_OK;
extern int const RESULT_CANCEL;
