//
//  AppDelegate.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "AppHelper.h"

#import <MobbIDFramework/MobbIDFramework.h>

NSString *const BASE_URL = @"http://test3.mobbeel.com";
NSString *const LICENSE_KEY = @"";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	// Override point for customization after application launch.
    
	MainViewController *viewController = [[MainViewController alloc] initWithNibName:[AppHelper appendPlatform:@"MainViewController"] bundle:nil];
    
	self.navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
	self.window.rootViewController = self.navController;
    
	[self.window makeKeyAndVisible];
	return YES;
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
	NSUInteger orientations = UIInterfaceOrientationMaskAllButUpsideDown;
    
	if (self.window.rootViewController) {
		UIViewController *presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
		orientations = [presentedViewController supportedInterfaceOrientations];
	}
    
	return orientations;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	/* Init with the licenseId provided by Mobbeel. */
	[[MobbIDAPI getInstance] initAPIWithLicense:LICENSE_KEY];
    
	/* Change it if you have your own test MobbID's server instance. The default value is http://test3.mobbeel.com */
	[[MobbIDAPI getInstance] setBaseURL:BASE_URL];
    
	/*
     Set to YES if you want to perform the biometric recognition offline. The default value is NO.
     Check the documentation to see what methods are available in offline mode.
	 */
	[[MobbIDAPI getInstance] setApiMode:MobbIDAPIMode_ONLINE];
	if ([LICENSE_KEY isEqualToString:@""]) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil)
		                                                    message:NSLocalizedString(@"CONFIGURE_LICENSE", nil)
		                                                   delegate:self
		                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
		                                          otherButtonTitles:nil];
		[alertView show];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
