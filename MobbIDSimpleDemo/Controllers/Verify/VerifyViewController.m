//
//  VerifyViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "VerifyViewController.h"
#import "AppHelper.h"
#import "Constants.h"
#import "VoiceViewController.h"
#import "FaceViewController.h"
#import "SignatureViewController.h"
#import "IrisViewController.h"
#import "FaceVoiceViewController.h"

@interface VerifyViewController () <ResultViewDelegate, RetrieveUserInformationDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnFace;
@property (nonatomic, weak) IBOutlet UIButton *btnSignature;
@property (nonatomic, weak) IBOutlet UIButton *btnVoice;
@property (nonatomic, weak) IBOutlet UIButton *btnIris;
@property (nonatomic, weak) IBOutlet UIButton *btnFaceVoice;

- (IBAction)voiceTouched:(id)sender;
- (IBAction)faceTouched:(id)sender;
- (IBAction)signatureTouched:(id)sender;
- (IBAction)irisTouched:(id)sender;
- (IBAction)faceVoiceTouched:(id)sender;

- (void)setupButtonStateOnCreate:(UIButton *)button method:(NSString *)method enrolledMethods:(NSArray *)enrolledMethods grantedMethods:(NSArray *)grantedMethods;

@end

@implementation VerifyViewController

- (void)viewDidLoad {
	[super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
	[self.mobbIDManagementAPI retrieveUserInfo:self.userId delegate:self];
	[self.btnFace setTitle:NSLocalizedString(self.btnFace.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnSignature setTitle:NSLocalizedString(self.btnSignature.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnVoice setTitle:NSLocalizedString(self.btnVoice.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnIris setTitle:NSLocalizedString(self.btnIris.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnFaceVoice setTitle:NSLocalizedString(self.btnFaceVoice.titleLabel.text, nil) forState:UIControlStateNormal];
}

- (IBAction)voiceTouched:(id)sender {
	VoiceViewController *voiceVC = [[VoiceViewController alloc] initWithNibName:[AppHelper appendPlatform:@"VoiceViewController"] bundle:nil];
	voiceVC.delegate = self;
	voiceVC.userId = self.userId;
	voiceVC.recognitionMode = MobbIDSDKRecognitionMode_Verification;
	voiceVC.language = [AppHelper languageFromDevice];
	[self.navigationController pushViewController:voiceVC animated:YES];
}

- (IBAction)faceTouched:(id)sender {
	FaceViewController *faceVC = [[FaceViewController alloc] initWithNibName:[AppHelper appendPlatform:@"FaceViewController"] bundle:nil];
	faceVC.delegate = self;
	faceVC.userId = self.userId;
	faceVC.recognitionMode = MobbIDSDKRecognitionMode_Verification;
	[self.navigationController pushViewController:faceVC animated:YES];
}

- (IBAction)signatureTouched:(id)sender {
	SignatureViewController *signatureVC = [[SignatureViewController alloc] initWithNibName:[AppHelper appendPlatform:@"SignatureViewController"] bundle:nil];
	signatureVC.delegate = self;
	signatureVC.userId = self.userId;
	signatureVC.recognitionMode = MobbIDSDKRecognitionMode_Verification;
	[self.navigationController pushViewController:signatureVC animated:YES];
}

- (IBAction)irisTouched:(id)sender {
	IrisViewController *irisVC = [[IrisViewController alloc] initWithNibName:[AppHelper appendPlatform:@"IrisViewController"] bundle:nil];
	irisVC.delegate = self;
	irisVC.userId = self.userId;
	irisVC.recognitionMode = MobbIDSDKRecognitionMode_Verification;
	[self.navigationController pushViewController:irisVC animated:YES];
}

- (IBAction)faceVoiceTouched:(id)sender {
	FaceVoiceViewController *faceVoiceVC = [[FaceVoiceViewController alloc] initWithNibName:[AppHelper appendPlatform:@"FaceVoiceViewController"] bundle:nil];
	faceVoiceVC.delegate = self;
	faceVoiceVC.userId = self.userId;
	faceVoiceVC.recognitionMode = MobbIDSDKRecognitionMode_Verification;
	faceVoiceVC.language = [AppHelper languageFromDevice];
	[self.navigationController pushViewController:faceVoiceVC animated:YES];
}

#pragma mark - mobbIDViewDelegate method

- (void)mobbIDViewEnrollmentFinishedWithResult:(MobbIDSDKOperationEnrollmentResult)resultCode method:(MobbIDSDKBiometricMethod)method error:(NSError *)errorOccurred {
	NSString *message;
	switch (resultCode) {
		case MobbIDSDKOperationEnrollmentResult_USER_ENROLLED:
			self.navigationItem.rightBarButtonItem.enabled = YES;
			switch (method) {
				case MobbIDSDKBiometricMethod_METHOD_IRIS:
					self.btnIris.enabled = NO;
					self.btnIris.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_FACE:
					self.btnFace.enabled = NO;
					self.btnFace.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_SIGNATURE:
					self.btnSignature.enabled = NO;
					self.btnSignature.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_VOICE:
					self.btnVoice.enabled = NO;
					self.btnVoice.alpha = 0.5f;
                    
					break;
                    
				default:
					break;
			}
			message = NSLocalizedString(@"USER_ENROLLED", nil);
			break;
            
		case MobbIDSDKOperationEnrollmentResult_ERROR:
		default:
			message = NSLocalizedString(@"ERROR_OCURRED", nil);
			break;
	}
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ACCEPT", nil) otherButtonTitles:nil];
	[alertView show];
}

- (void)mobbIDViewVerificationFinishedWithResult:(MobbIDSDKOperationVerificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                           token:(NSString *)token
                                           error:(NSError *)errorOccurred {
	NSString *message;
	switch (resultCode) {
		case MobbIDSDKOperationVerificationResult_USER_VERIFIED:
			self.navigationItem.rightBarButtonItem.enabled = YES;
			switch (method) {
				case MobbIDSDKBiometricMethod_METHOD_IRIS:
					self.btnIris.enabled = NO;
					self.btnIris.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_FACE:
					self.btnFace.enabled = NO;
					self.btnFace.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_SIGNATURE:
					self.btnSignature.enabled = NO;
					self.btnSignature.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_VOICE:
					self.btnVoice.enabled = NO;
					self.btnVoice.alpha = 0.5f;
                    
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_VOICE_FACE:
					self.btnFaceVoice.enabled = NO;
					self.btnFaceVoice.alpha = 0.5f;
                    
					break;
                    
				default:
					break;
			}
			message = NSLocalizedString(@"USER_VERIFIED", nil);
            
			break;
            
		case MobbIDSDKOperationVerificationResult_USER_NOT_VERIFIED:
			message = NSLocalizedString(@"USER_NOT_VERIFIED", nil);
			break;
            
		case MobbIDSDKOperationVerificationResult_ERROR:
		default:
			message = NSLocalizedString(@"ERROR_OCURRED", nil);
			break;
	}
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ACCEPT", nil) otherButtonTitles:nil];
	[alertView show];
}

- (void)retrieveUserInformationFinished:(MobbIDAPIRetrieveUserInformationResult)result
                                forUser:(NSString *)userId
                        enrolledMethods:(NSArray *)enrolledMethods
                   authenticatedMethods:(NSArray *)authenticatedMethods
                         grantedMethods:(NSArray *)grantedMethods
                                  error:(NSError *)errorOccurred {
	if (result == MobbIDAPIRetrieveUserInformationResult_OK) {
		[self setupButtonStateOnCreate:self.btnFace method:METHOD_FACE enrolledMethods:enrolledMethods grantedMethods:grantedMethods];
		[self setupButtonStateOnCreate:self.btnSignature method:METHOD_SIGNATURE enrolledMethods:enrolledMethods grantedMethods:grantedMethods];
		[self setupButtonStateOnCreate:self.btnVoice method:METHOD_VOICE enrolledMethods:enrolledMethods grantedMethods:grantedMethods];
		[self setupButtonStateOnCreate:self.btnIris method:METHOD_IRIS enrolledMethods:enrolledMethods grantedMethods:grantedMethods];
		// Voice+Face method
		if ([grantedMethods containsObject:METHOD_VOICE_FACE] && [enrolledMethods containsObject:METHOD_FACE]
		    && [enrolledMethods containsObject:METHOD_VOICE]) {
			self.btnFaceVoice.enabled = YES;
			self.btnFaceVoice.alpha = 1.0f;
		}
		else {
			self.btnFaceVoice.enabled = NO;
			self.btnFaceVoice.alpha = 0.5;
		}
	}
	else if (result == MobbIDAPIRetrieveUserInformationResult_ERROR && errorOccurred) {
		NSLog(@"The information about the user cannot be retrieved. Error code: %d", [errorOccurred code]);
		NSDictionary *dict = [self handleCommonMobbIDSDKErrors:errorOccurred];
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"title"]
		                                                    message:[dict objectForKey:@"message"]
		                                                   delegate:self
		                                          cancelButtonTitle:@"OK"
		                                          otherButtonTitles:nil];
		[alertView show];
	}
}

- (void)setupButtonStateOnCreate:(UIButton *)button method:(NSString *)method enrolledMethods:(NSArray *)enrolledMethods grantedMethods:(NSArray *)grantedMethods {
	if ([grantedMethods containsObject:method] && [enrolledMethods containsObject:method]) {
		button.enabled = YES;
		button.alpha = 1.0f;
	}
	else {
		button.enabled = NO;
		button.alpha = 0.5f;
	}
}

@end
