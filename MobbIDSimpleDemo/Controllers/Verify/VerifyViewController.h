//
//  VerifyViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface VerifyViewController : BaseViewController

@property (nonatomic, weak) NSString *userId;

@end
