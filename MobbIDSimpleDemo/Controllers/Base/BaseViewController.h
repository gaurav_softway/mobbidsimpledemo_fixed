//
//  DemoViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 11/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MobbIDFramework/MobbIDFramework.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong, readonly) MobbIDManagementAPI *mobbIDManagementAPI;

- (void)showProgress;
- (void)hideProgress;

- (NSDictionary *)handleCommonMobbIDSDKErrors:(NSError *)error;
- (void)showError:(NSError *)error;

@end
