//
//  MainViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 11/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"

@interface BaseViewController ()

@property (nonatomic, strong, readwrite) MobbIDManagementAPI *mobbIDManagementAPI;

@end

@implementation BaseViewController

@synthesize mobbIDManagementAPI;

// lazy initialization of the property
- (MobbIDManagementAPI *)mobbIDManagementAPI {
	if (mobbIDManagementAPI) {
		return mobbIDManagementAPI;
	}
	mobbIDManagementAPI = [[MobbIDManagementAPI alloc] init];
	return mobbIDManagementAPI;
}

- (void)showProgress {
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)hideProgress {
	[MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (NSDictionary *)handleCommonMobbIDSDKErrors:(NSError *)error {
	NSString *title;
	NSString *message;
	switch ([error code]) {
		case MobbIDSDKErrorCode_ERROR_CONNECTION:
			title = @"ERROR_CONNECTION_TITLE";
			message = @"ERROR_CONNECTION_TEXT";
			break;
            
		case MobbIDSDKErrorCode_ERROR_NOT_AVAILABLE_IN_CURRENT_MODE:
			title = @"ERROR_NOT_AVAILABLE_IN_CURRENT_MODE";
			message = @"ERROR_NOT_AVAILABLE_IN_CURRENT_MODE";
			break;
            
		case MobbIDSDKErrorCode_ERROR_USER_DOES_NOT_EXIST:
			title = @"ERROR_USER_DOES_NOT_EXIST_TITLE";
			message = @"ERROR_USER_DOES_NOT_EXIST_TEXT";
			break;
            
		case MobbIDSDKErrorCode_ERROR_LICENSE:
			title = @"ERROR_LICENSE_GENERIC_TITLE";
			message = @"ERROR_LICENSE_GENERIC_TEXT";
			break;
            
		case MobbIDSDKErrorCode_ERROR_UNEXPECTED:
			title = NSLocalizedString(@"UNEXPECTED_ERROR_TITLE", nil);
			message = NSLocalizedString(@"UNEXPECTED_ERROR_TEXT", nil);
			break;
            
		default:
			title = [[error userInfo] objectForKey:NSLocalizedFailureReasonErrorKey];
			message = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
			break;
	}
    
	// XXX Problems with iOS < 6.0 ??
	NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[title, message] forKeys:@[@"title", @"message"]];
	return dict;
}

- (void)showError:(NSError *)error {
	NSDictionary *dict = [self handleCommonMobbIDSDKErrors:error];
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"title"]
	                                                    message:[dict objectForKey:@"message"]
	                                                   delegate:self
	                                          cancelButtonTitle:@"OK"
	                                          otherButtonTitles:nil];
	[alertView show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
	// Return YES for supported orientations
	return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskPortrait; // etc
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	return UIInterfaceOrientationPortrait;
}

@end
