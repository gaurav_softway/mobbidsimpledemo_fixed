//
//  FaceVoiceViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 02/05/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "FaceVoiceViewController.h"

@implementation FaceVoiceViewController

- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_VOICE_FACE;
        // standard alloc/init with frame call for a uiView subclass
        self.mobbIDView = [[FaceVoiceView alloc] initWithFrame:self.view.bounds];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        // we set the language
        [self.mobbIDView setLanguage:MobbIDAPISupportedLanguage_English];
        // we set the flac encoding
        [self.mobbIDView setUseFlacEncoding:NO];
        //we set the speech mode
        [self.mobbIDView setSpeechMode:MobbIDSDKVoiceRecognitionSpeechMode_Passphrase];
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
}

@end
