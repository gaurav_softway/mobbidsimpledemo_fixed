//
//  FaceVoiceViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 02/05/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMobbIDViewController.h"

@interface FaceVoiceViewController : BaseMobbIDViewController

@property (nonatomic) MobbIDSDKVoiceRecognitionSpeechMode random;
@property (nonatomic) MobbIDAPISupportedLanguage language;

@end
