//
//  VoiceViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "VoiceViewController.h"

@implementation VoiceViewController

- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_VOICE;
        
        // standard alloc/init with frame call for a uiView subclass
        self.mobbIDView = [[VoiceView alloc] initWithFrame:self.view.bounds];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        // setup Verification
        if (self.recognitionMode != MobbIDSDKRecognitionMode_Enrollment) {
            [self.mobbIDView setVerificationType:MobbIDSDKVerificationType_Simple];
            [self.mobbIDView setOperationId:nil];
        }
        // we set the language
        [self.mobbIDView setLanguage:MobbIDAPISupportedLanguage_English];
        // we set the flac encoding
        [self.mobbIDView setUseFlacEncoding:NO];
        //we set the speech mode
        [self.mobbIDView setSpeechMode:MobbIDSDKVoiceRecognitionSpeechMode_Passphrase];
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
}

@end
