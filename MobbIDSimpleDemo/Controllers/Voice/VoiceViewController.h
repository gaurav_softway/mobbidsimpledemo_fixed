//
//  VoiceViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMobbIDViewController.h"

@interface VoiceViewController : BaseMobbIDViewController

@property (nonatomic) MobbIDSDKVoiceRecognitionSpeechMode random;
@property (nonatomic) MobbIDAPISupportedLanguage language;

@end
