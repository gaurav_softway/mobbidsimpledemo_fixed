//
//  SignatureViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 09/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "SignatureViewController.h"

//#import "PogoPenManager.h"

@interface SignatureViewController () <ActivePenManagerDelegate>

@end

@implementation SignatureViewController

- (void)createView {
    
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_SIGNATURE;
    
        // standard alloc/init with frame call for a uiView subclass
        self.mobbIDView = [[SignatureView alloc] initWithFrame:self.view.bounds];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
    
        NSError *error = nil;
        if (![self.mobbIDView addCustomizationProperty:[NSNumber numberWithInt:3] forKey:@"signature.stroke.width" error:&error]) {
            NSLog(@"Error customizing the signature: %@", [error description]);
        }
        if (![self.mobbIDView addCustomizationProperty:[UIColor blueColor] forKey:@"signature.stroke.color" error:&error]) {
            NSLog(@"Error customizing the signature: %@", [error description]);
        }
        
        // Removing default grid background for signature view
        if (![self.mobbIDView addCustomizationProperty:nil forKey:@"signature.background.image" error:&error]) {
            NSLog(@"Error customizing the signature: %@", [error description]);
        }
        
//        Customizing the background image for signature view
//        if (![self.mobbIDView addCustomizationProperty:[UIImage imageNamed:@"bg_sign"] forKey:@"signature.background.image" error:&error]) {
//            NSLog(@"Error customizing the signature: %@", [error description]);
//        }
        
//        Customizing the background color for the signature view
//        if (![self.mobbIDView addCustomizationProperty:[UIColor greenColor] forKey:@"signature.background.color" error:&error]) {
//            NSLog(@"Error customizing the signature: %@", [error description]);
//        }
    
        // disable iOS 7 Interactive Pop Gesture Recognizer
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }

//        Using Pogo as the active pen
//        [PogoPenManager alloc];
//        [self.mobbIDView setActivePenType:ActivePenTypePogo];
//        [self.mobbIDView setActivePen:YES];
//        [self.mobbIDView setActivePenDelegate:self];
        
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return YES;
}

- (BOOL)shouldAutorotate {
	// Return YES for supported orientations
	return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskAll; // etc
}


//#pragma mark -- ActivePenManagerDelegate's methods
//
//- (void) onSearchingForPen {
//    NSLog(@"onSearchingForPen");
//}
//
//- (void) activePenDidConnect:(ActivePenType)penType {
//    NSLog(@"activePenDidConnect");
//}
//
//- (void) activePenDidDisconnect:(ActivePenType)penType {
//    NSLog(@"activePenDidDisconnect");
//}
//
//- (void) activePenNotSupported:(ActivePenType)penType {
//    NSLog(@"activePenNotSupported");
//}

@end
