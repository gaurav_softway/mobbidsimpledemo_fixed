//
//  SignatureViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 09/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMobbIDViewController.h"

@interface SignatureViewController : BaseMobbIDViewController

@end
