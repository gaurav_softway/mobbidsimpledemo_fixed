//
//  CreateViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "CreateViewController.h"
#import "AppHelper.h"
#import "Constants.h"
#import "VoiceViewController.h"
#import "FaceViewController.h"
#import "SignatureViewController.h"
#import "IrisViewController.h"

#import <MobbIDFramework/MobbIDFramework.h>

@interface CreateViewController () <ResultViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnFace;
@property (nonatomic, weak) IBOutlet UIButton *btnSignature;
@property (nonatomic, weak) IBOutlet UIButton *btnVoice;
@property (nonatomic, weak) IBOutlet UIButton *btnIris;

- (IBAction)voiceTouched:(id)sender;
- (IBAction)faceTouched:(id)sender;
- (IBAction)signatureTouched:(id)sender;
- (IBAction)irisTouched:(id)sender;
- (void)okTouched:(id)sender;
- (void)cancelTouched:(id)sender;

@end

@implementation CreateViewController

- (void)viewDidLoad {
	[super viewDidLoad];
    
	//setup buttons
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"SAVE", nil) style:UIBarButtonItemStylePlain target:self action:@selector(okTouched:)];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelTouched:)];
	self.navigationItem.rightBarButtonItem.enabled = NO;
	[self.btnFace setTitle:NSLocalizedString(self.btnFace.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnSignature setTitle:NSLocalizedString(self.btnSignature.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnVoice setTitle:NSLocalizedString(self.btnVoice.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnIris setTitle:NSLocalizedString(self.btnIris.titleLabel.text, nil) forState:UIControlStateNormal];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (IBAction)voiceTouched:(id)sender {
	VoiceViewController *voiceVC = [[VoiceViewController alloc] initWithNibName:[AppHelper appendPlatform:@"VoiceViewController"] bundle:nil];
	voiceVC.delegate = self;
	voiceVC.userId = self.userId;
	voiceVC.recognitionMode = MobbIDSDKRecognitionMode_Enrollment;
	voiceVC.language = [AppHelper languageFromDevice];
	[self.navigationController pushViewController:voiceVC animated:YES];
}

- (IBAction)faceTouched:(id)sender {
	FaceViewController *faceVC = [[FaceViewController alloc] initWithNibName:[AppHelper appendPlatform:@"FaceViewController"] bundle:nil];
	faceVC.delegate = self;
	faceVC.userId = self.userId;
	faceVC.recognitionMode = MobbIDSDKRecognitionMode_Enrollment;
	[self.navigationController pushViewController:faceVC animated:YES];
}

- (IBAction)signatureTouched:(id)sender {
	SignatureViewController *signatureVC = [[SignatureViewController alloc] initWithNibName:[AppHelper appendPlatform:@"SignatureViewController"] bundle:nil];
	signatureVC.delegate = self;
	signatureVC.userId = self.userId;
	signatureVC.recognitionMode = MobbIDSDKRecognitionMode_Enrollment;
	[self.navigationController pushViewController:signatureVC animated:YES];
}

- (IBAction)irisTouched:(id)sender {
	IrisViewController *irisVC = [[IrisViewController alloc] initWithNibName:[AppHelper appendPlatform:@"IrisViewController"] bundle:nil];
	irisVC.delegate = self;
	irisVC.userId = self.userId;
	irisVC.recognitionMode = MobbIDSDKRecognitionMode_Enrollment;
	[self.navigationController pushViewController:irisVC animated:YES];
}

- (void)okTouched:(id)sender {
	[self.delegate createVCFinishedWithResult:RESULT_OK];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelTouched:(id)sender {
	[self.delegate createVCFinishedWithResult:RESULT_CANCEL];
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ResultViewDelegate method

- (void)mobbIDViewEnrollmentFinishedWithResult:(MobbIDSDKOperationEnrollmentResult)resultCode method:(MobbIDSDKBiometricMethod)method error:(NSError *)errorOccurred {
	NSString *message;
	switch (resultCode) {
		case MobbIDSDKOperationEnrollmentResult_USER_ENROLLED:
			self.navigationItem.rightBarButtonItem.enabled = YES;
			switch (method) {
				case MobbIDSDKBiometricMethod_METHOD_IRIS:
					self.btnIris.enabled = NO;
					self.btnIris.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_FACE:
					self.btnFace.enabled = NO;
					self.btnFace.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_SIGNATURE:
					self.btnSignature.enabled = NO;
					self.btnSignature.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_VOICE:
					self.btnVoice.enabled = NO;
					self.btnVoice.alpha = 0.5f;
					break;
                    
				default:
					break;
			}
			message = NSLocalizedString(@"USER_ENROLLED", nil);
			break;
            
		case MobbIDSDKOperationEnrollmentResult_ERROR:
		default:
			message = NSLocalizedString(@"ERROR_OCURRED", nil);
			break;
	}
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ACCEPT", nil) otherButtonTitles:nil];
	[alertView show];
}

- (void)mobbIDViewVerificationFinishedWithResult:(MobbIDSDKOperationVerificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                           token:(NSString *)token
                                           error:(NSError *)errorOccurred {
	NSString *message;
	switch (resultCode) {
		case MobbIDSDKOperationVerificationResult_USER_VERIFIED:
			self.navigationItem.rightBarButtonItem.enabled = YES;
			switch (method) {
				case MobbIDSDKBiometricMethod_METHOD_IRIS:
					self.btnIris.enabled = NO;
					self.btnIris.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_FACE:
					self.btnFace.enabled = NO;
					self.btnFace.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_SIGNATURE:
					self.btnSignature.enabled = NO;
					self.btnSignature.alpha = 0.5f;
					break;
                    
				case MobbIDSDKBiometricMethod_METHOD_VOICE:
					self.btnVoice.enabled = NO;
					self.btnVoice.alpha = 0.5f;
                    
					break;
                    
				default:
					break;
			}
			message = NSLocalizedString(@"USER_VERIFIED", nil);
            
			break;
            
		case MobbIDSDKOperationVerificationResult_USER_NOT_VERIFIED:
			message = NSLocalizedString(@"USER_NOT_VERIFIED", nil);
            
		case MobbIDSDKOperationVerificationResult_ERROR:
		default:
			message = NSLocalizedString(@"ERROR_OCURRED", nil);
			break;
	}
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:NSLocalizedString(@"ACCEPT", nil) otherButtonTitles:nil];
	[alertView show];
}

@end
