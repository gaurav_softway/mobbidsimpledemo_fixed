//
//  CreateViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@protocol CreateVCDelegate <NSObject>

- (void)createVCFinishedWithResult:(int)resultCode;

@end

@interface CreateViewController : BaseViewController

@property (nonatomic, weak) NSString *userId;
@property (nonatomic, weak) id <CreateVCDelegate> delegate;

@end
