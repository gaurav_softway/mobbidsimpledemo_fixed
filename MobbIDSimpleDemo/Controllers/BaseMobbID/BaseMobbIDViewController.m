//
//  BaseMobbIDViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "BaseMobbIDViewController.h"


@interface BaseMobbIDViewController ()

@end

@implementation BaseMobbIDViewController

- (void)viewDidLoad {
	[super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)createView {
	//redefine in subclass
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createView];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	NSLog(@"Start user recognition process");
	[self.mobbIDView startRecognitionIn:self.recognitionMode forUser:self.userId];
}

- (void)viewWillDisappear:(BOOL)animated {
	NSLog(@"Stop user recognition process");
	[self.mobbIDView stopRecognition];
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
	[super viewWillDisappear:animated];
}

#pragma mark - BiometricMethodDelegate methods

- (void) enrollmentFinished:(MobbIDSDKOperationEnrollmentResult)resultCode
                       data:(MobbIDSDKOperationEnrollmentResultData *)data
                      error:(NSError *)errorOccurred {
	NSLog(@"Enrollment finished with resultCode = %d", resultCode);
	[self.delegate mobbIDViewEnrollmentFinishedWithResult:resultCode method:self.method error:errorOccurred];
	[self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

- (void) verificationFinished:(MobbIDSDKOperationVerificationResult)resultCode
                         data:(MobbIDSDKOperationVerificationResultData *)data
                        error:(NSError *)errorOccurred {
	NSLog(@"Verification finished with resultCode = %d", resultCode);
	[self.delegate mobbIDViewVerificationFinishedWithResult:resultCode method:data.method token:data.token error:errorOccurred];
	[self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
	return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	return UIInterfaceOrientationPortrait;
}

@end
