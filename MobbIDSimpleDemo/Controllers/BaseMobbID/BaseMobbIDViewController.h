//
//  BaseMobbIDViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 12/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MobbIDFramework/MobbIDFramework.h>

@protocol ResultViewDelegate <NSObject>

- (void)mobbIDViewEnrollmentFinishedWithResult:(MobbIDSDKOperationEnrollmentResult)resultCode
                                        method:(MobbIDSDKBiometricMethod)method
                                         error:(NSError *)errorOccurred;

- (void)mobbIDViewVerificationFinishedWithResult:(MobbIDSDKOperationVerificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                           token:(NSString *)token
                                           error:(NSError *)errorOccurred;

@end

@interface BaseMobbIDViewController : UIViewController <BiometricMethodDelegate>

@property (nonatomic, strong) id mobbIDView;

@property (nonatomic, weak) NSString *userId;
@property (nonatomic) MobbIDSDKRecognitionMode recognitionMode;
@property (nonatomic, weak) id <ResultViewDelegate> delegate;
@property (nonatomic) MobbIDSDKBiometricMethod method;

- (void)createView;

@end
