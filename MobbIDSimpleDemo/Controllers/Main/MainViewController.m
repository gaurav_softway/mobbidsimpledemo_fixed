//
//  ViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "MainViewController.h"
#import "CreateViewController.h"
#import "VerifyViewController.h"
#import "AppHelper.h"
#import "Constants.h"

#import <MobbIDFramework/MobbIDFramework.h>

@interface MainViewController () <CreateVCDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnCreate;
@property (nonatomic, weak) IBOutlet UIButton *btnVerify;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@property (nonatomic, strong) NSString *userId;

- (IBAction)createTouched:(id)sender;
- (IBAction)verifyTouched:(id)sender;
- (IBAction)deleteTouched:(id)sender;

- (void)setupButtonsState;
- (void)showLogoutDeleteUserAlertView;
- (void)deleteUser;

@end

@implementation MainViewController

- (void)viewDidLoad {
	[super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
	[self setupButtonsState];
	[self.btnCreate setTitle:NSLocalizedString(self.btnCreate.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnVerify setTitle:NSLocalizedString(self.btnVerify.titleLabel.text, nil) forState:UIControlStateNormal];
	[self.btnDelete setTitle:NSLocalizedString(self.btnDelete.titleLabel.text, nil) forState:UIControlStateNormal];
}

- (void)setupButtonsState {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL userCreated = [defaults objectForKey:EXTRA_USER_ID] != nil;
	self.btnCreate.hidden = userCreated ? YES : NO;
	self.btnVerify.hidden = userCreated ? NO : YES;
	self.btnDelete.hidden = userCreated ? NO : YES;
	if (userCreated) {
		self.userId = [defaults objectForKey:EXTRA_USER_ID];
	}
}

- (IBAction)createTouched:(id)sender {
	[self showProgress];
	[self.mobbIDManagementAPI createUserWithLanguage:MobbIDAPISupportedLanguage_English andGender:MobbIDAPIGender_Male delegate:self];
}

- (IBAction)verifyTouched:(id)sender {
	if (self.userId != nil && [self.userId length] > 0) {
		VerifyViewController *verifyVC = [[VerifyViewController alloc] initWithNibName:[AppHelper appendPlatform:@"VerifyViewController"] bundle:nil];
		verifyVC.userId = self.userId;
		[self.navigationController pushViewController:verifyVC animated:YES];
	}
}

- (IBAction)deleteTouched:(id)sender {
	if (self.userId != nil && [self.userId length] > 0) {
		[self showLogoutDeleteUserAlertView];
	}
}

- (void)showLogoutDeleteUserAlertView {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"DELETE_USER", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:NSLocalizedString(@"ACCEPT", nil), nil];
	[alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		[self.mobbIDManagementAPI deleteUser:self.userId delegate:self];
	}
}

- (void)deleteUser {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults removeObjectForKey:EXTRA_USER_ID];
	[defaults synchronize];
	[self setupButtonsState];
}

#pragma mark - CreateUserDelegate method

- (void)createUserFinished:(MobbIDAPICreateUserResult)result forUser:(NSString *)userId token:(NSString *)token error:(NSError *)errorOccurred {
	[self hideProgress];
	if (result == MobbIDAPICreateUserResult_OK) {
		NSLog(@"User is created, but not yet confirmed. UserUUID: %@ , token: %@", userId, token);
		[self showProgress];
		[self.mobbIDManagementAPI confirmCreateUser:userId token:token delegate:self];
	}
	else {
		[self showError:errorOccurred];
	}
}

#pragma mark - ConfirmUserDelegate method

- (void)confirmCreateUserFinished:(MobbIDAPIConfirmUserResult)result forUser:(NSString *)userId error:(NSError *)errorOccurred {
	[self hideProgress];
	if (result == MobbIDAPIConfirmUserResult_OK) {
		self.userId = userId;
		NSLog(@"The user has been confirmed. Now you can enroll the user in any granted biometric method by your license");
		CreateViewController *createVC = [[CreateViewController alloc] initWithNibName:[AppHelper appendPlatform:@"CreateViewController"] bundle:nil];
		createVC.userId = self.userId;
		createVC.delegate = self;
		[self.navigationController pushViewController:createVC animated:YES];
	}
	else if (result == MobbIDAPIConfirmUserResult_ERROR && errorOccurred) {
		NSLog(@"Error while CONFIRMING user = %d", result);
		NSString *title = nil;
		NSString *message = nil;
		switch ([errorOccurred code]) {
			case ERROR_TOKEN_IS_NOT_VALID:
			case ERROR_USER_AND_TOKEN_COMBINATION_NOT_VALID:
				NSLog(@"The token is not the one created for the user. Error code: %d", [errorOccurred code]);
				title = NSLocalizedString(@"INVALID_TOKEN_TITLE", nil);
				message = NSLocalizedString(@"INVALID_TOKEN_TEXT", nil);
				break;
                
			case ERROR_TOKEN_HAS_EXPIRED:
				NSLog(@"The validity of the token has expired. Error code: %d", [errorOccurred code]);
				title = NSLocalizedString(@"EXPIRED_TOKEN_TITLE", nil);
				message = NSLocalizedString(@"EXPIRED_TOKEN_TEXT", nil);
                
			default:
				// Common errors
				NSLog(@"Generic error in the creation. Error code: %d", [errorOccurred code]);
				NSDictionary *dict = [self handleCommonMobbIDSDKErrors:errorOccurred];
				title = [dict objectForKey:@"title"];
				message = [dict objectForKey:@"message"];
				break;
		}
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
		                                                    message:message
		                                                   delegate:self
		                                          cancelButtonTitle:@"OK"
		                                          otherButtonTitles:nil];
		[alertView show];
	}
}

#pragma mark - LogoutUserDelegate method

- (void)logoutFinished:(MobbIDAPILogoutUserResult)result forUser:(NSString *)userId error:(NSError *)errorOccurred {
	[self hideProgress];
	if (result == MobbIDAPILogoutUserResult_OK) {
		NSLog(@"User is logout, but not yet deleted");
		[self showProgress];
		[self.mobbIDManagementAPI deleteUser:self.userId delegate:self];
	}
	else if (result == MobbIDAPILogoutUserResult_ERROR && errorOccurred) {
		NSLog(@"The user cannot be logged out. Error code: %d", [errorOccurred code]);
		NSDictionary *dict = [self handleCommonMobbIDSDKErrors:errorOccurred];
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"title"]
		                                                    message:[dict objectForKey:@"message"]
		                                                   delegate:self
		                                          cancelButtonTitle:@"OK"
		                                          otherButtonTitles:nil];
		[alertView show];
	}
}

#pragma mark - DeleteUserDelegate method

- (void)deleteUserFinished:(MobbIDAPIDeleteUserDelegateResult)result forUser:(NSString *)userId error:(NSError *)errorOccurred {
	[self hideProgress];
	if (result == MobbIDAPIDeleteUserDelegateResult_OK) {
		NSLog(@"User deleted successfully: %@", userId);
		[self deleteUser];
	}
	else if (result == MobbIDAPIDeleteUserDelegateResult_ERROR) {
		NSLog(@"Error during DELETE process. Error code %d", [errorOccurred code]);
		NSDictionary *dict = [self handleCommonMobbIDSDKErrors:errorOccurred];
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"title"]
		                                                    message:[dict objectForKey:@"message"]
		                                                   delegate:self
		                                          cancelButtonTitle:@"OK"
		                                          otherButtonTitles:nil];
		[alertView show];
	}
}

#pragma mark - CreateVCDelegate method

- (void)createVCFinishedWithResult:(int)resultCode {
	if (resultCode == RESULT_OK) {
		self.btnCreate.hidden = YES;
		self.btnVerify.hidden = NO;
		self.btnDelete.hidden = NO;
		// store user for future verifications
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:self.userId forKey:EXTRA_USER_ID];
		[defaults synchronize];
	}
	else {
		[self showProgress];
		[self.mobbIDManagementAPI deleteUser:self.userId delegate:self];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
