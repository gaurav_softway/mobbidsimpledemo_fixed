//
//  ViewController.h
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

extern const int DELETE;

@interface MainViewController : BaseViewController <CreateUserDelegate, ConfirmUserDelegate, LogoutUserDelegate, DeleteUserDelegate, UIAlertViewDelegate>

@end
