//
//  FaceDetectorViewController.m
//  MobbIDSimpleDemo
//
//  Created by Raul Jareño diaz on 08/04/13.
//  Copyright (c) 2013 Mobbeel. All rights reserved.
//

#import "FaceViewController.h"

@implementation FaceViewController

- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_FACE;
        // standard alloc/init with frame call for a UIView subclass
        self.mobbIDView = [[FaceView alloc] initWithFrame:self.view.bounds];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
}

@end
